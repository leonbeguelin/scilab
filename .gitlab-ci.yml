# Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
# Copyright (C) Dassault Systemes - 2022 - Clement DAVID
# Copyright (C) Dassault Systemes - 2022 - Cedric DELAMARRE

stages:
  # refresh the Docker image
  - docker_create
  # define common env variables used to build and tests
  - prebuild
  # build scilab
  - build
  # run the tests
  - test
  # extract some content
  - postbuild
  # publish and make it available to the community
  - publish
  # update/cleanup Docker image
  - docker_cleanup

variables:
  GIT_STRATEGY:
    value: "clone"
    description: "fastest strategy as it re-uses the local working copy"
  GIT_CLEAN_FLAGS:
    value: "-fxd"
    description: "do clean build, each job should clean previous artifacts at startup"
  GIT_DEPTH:
    value: "1"
    description: "speedup cloning, only fetch the latest commit"
  BRANCH:
    value: "main"
    description: "Scilab branch name to build"
  DOCKER_LINUX_BUILDER:
    value: "registry.gitlab.com/scilab/scilab/linux-builder-main"
    description: "Docker image used to build Scilab on Linux"
  DOCKER_WINDOWS_BUILDER:
    value: "registry.gitlab.com/scilab/scilab/windows-builder-main"
    description: "Docker image used to build Scilab on Windows"
  SCILAB_COMMON_PATH:
    value: "${CI_PROJECT_DIR}/../../../../scilab/"
    description: "common windows path for persistency"
workflow:
  rules:
    # Run on contributor's fork
    - if: $CI_PROJECT_PATH_SLUG !~ /scilab-scilab/
      changes: &DOCKER_CHANGES
        paths:
          - .gitlab-ci/Dockerfile*
      variables:
        GIT_STRATEGY: "fetch"
        GIT_CLEAN_FLAGS: "none"
        SCI_VERSION_STRING: "${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_SHORT_SHA}"
        DOCKER_LINUX_BUILDER: "${CI_REGISTRY_IMAGE}/linux-builder-${BRANCH}:${CI_COMMIT_BRANCH}"
        DOCKER_WINDOWS_BUILDER: "${CI_REGISTRY_IMAGE}/windows-builder-${BRANCH}:${CI_COMMIT_BRANCH}"
    - if: $CI_PROJECT_PATH_SLUG !~ /scilab-scilab/
      variables:
        GIT_STRATEGY: "fetch"
        GIT_CLEAN_FLAGS: "none"
        SCI_VERSION_STRING: "${CI_PROJECT_PATH_SLUG}-${CI_COMMIT_SHORT_SHA}"
    # run on merge request and set specific docker image tag
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes: *DOCKER_CHANGES
      variables:
        GIT_STRATEGY: "fetch"
        GIT_CLEAN_FLAGS: "none"
        SCI_VERSION_STRING: "scilab-mr${CI_MERGE_REQUEST_IID}-${CI_COMMIT_SHORT_SHA}"
        BRANCH: "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
        DOCKER_LINUX_BUILDER: "${CI_REGISTRY_IMAGE}/linux-builder-${BRANCH}:mr${CI_MERGE_REQUEST_IID}"
        DOCKER_WINDOWS_BUILDER: "${CI_REGISTRY_IMAGE}/windows-builder-${BRANCH}:mr${CI_MERGE_REQUEST_IID}"
    # run on merge request
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      variables:
        GIT_STRATEGY: "fetch"
        GIT_CLEAN_FLAGS: "none"
        SCI_VERSION_STRING: "scilab-mr${CI_MERGE_REQUEST_IID}-${CI_COMMIT_SHORT_SHA}"
        BRANCH: "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
        DOCKER_LINUX_BUILDER: "${CI_REGISTRY_IMAGE}/linux-builder-${BRANCH}"
        DOCKER_WINDOWS_BUILDER: "${CI_REGISTRY_IMAGE}/windows-builder-${BRANCH}"
    # Scheduled pipeline
    - if: $CI_PIPELINE_SOURCE == 'schedule'
      variables:
        SCI_VERSION_STRING: "scilab-branch-${CI_COMMIT_BRANCH}-${CI_COMMIT_SHORT_SHA}"
        BRANCH: "${CI_COMMIT_BRANCH}"
        DOCKER_LINUX_BUILDER: "${CI_REGISTRY_IMAGE}/linux-builder-${CI_COMMIT_BRANCH}"
        DOCKER_WINDOWS_BUILDER: "${CI_REGISTRY_IMAGE}/windows-builder-${CI_COMMIT_BRANCH}"
    # Release
    - if: $CI_COMMIT_TAG
      variables:
        SCI_VERSION_STRING: "scilab-${CI_COMMIT_TAG}"
        BRANCH: "${CI_COMMIT_BRANCH}"
        DOCKER_LINUX_BUILDER: "${CI_REGISTRY_IMAGE}/linux-builder-${CI_COMMIT_BRANCH}"
        DOCKER_WINDOWS_BUILDER: "${CI_REGISTRY_IMAGE}/windows-builder-${CI_COMMIT_BRANCH}"
    # Update default Docker image on merge
    - if: $CI_MERGE_REQUEST_APPROVED
      changes: *DOCKER_CHANGES

default:
  artifacts:
    expire_in: 1 day

include:
  # Setup Docker
  - local: .gitlab-ci/docker_setup.yml
  # updating files and runner states
  - local: .gitlab-ci/prebuild.yml
  # build
  - local: .gitlab-ci/build.yml
  # execute tests
  - local: .gitlab-ci/test.yml
  # sign and extract useful content
  - local: .gitlab-ci/postbuild.yml
  # deploy/publish a binary
  - local: .gitlab-ci/publish.yml
